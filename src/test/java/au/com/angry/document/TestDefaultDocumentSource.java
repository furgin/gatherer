package au.com.angry.document;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
public class TestDefaultDocumentSource
{
    @Mock
    DocumentLocation documentLocation;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLoadDocument() throws Exception
    {
        when(documentLocation.getDocument()).
                thenReturn(doc("resource/first.html")).
                thenReturn(doc("resource/second.html"));

        DocumentSource ds = new DefaultDocumentSource();
        DocumentHolder documentHolder = ds.load(documentLocation);

        assertNotNull(documentHolder.select("div#first"));  // not found triggers a load
        assertNotNull(documentHolder.select("div#second")); // found, no load
        assertNotNull(documentHolder.select("div#third"));  // not found triggers another load

        verify(documentLocation,times(2)).getDocument();
    }

    private Document doc(String resource) throws IOException
    {
        return Jsoup.parse(IOUtils.toString(TestDefaultDocumentSource.class.getClassLoader().getResourceAsStream(resource)));
    }
}
