package au.com.angry.document;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 */
public class ResourceDocumentLocation implements DocumentLocation
{
    private final ClassLoader classLoader;
    private final String resource;

    public ResourceDocumentLocation(ClassLoader classLoader, String resource)
    {
        this.classLoader = classLoader;
        this.resource = resource;
    }

    public ResourceDocumentLocation(String resource)
    {
        this.classLoader = ResourceDocumentLocation.class.getClassLoader();
        this.resource = resource;
    }

    public String getResource()
    {
        return resource;
    }

    public Document getDocument()
    {
        try
        {
            return Jsoup.parse(IOUtils.toString(classLoader.getResourceAsStream(resource)));
        }
        catch (IOException e)
        {
            throw new DocumentLocationException(e);
        }
    }
}
