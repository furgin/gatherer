package au.com.angry.gatherer;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class TestColour
{
    @Test
    public void testSingleColour() {
        assertArrayEquals(new Colour[]{Colour.WHITE}, Colour.parse("{White}"));
        assertArrayEquals(new Colour[]{Colour.RED}, Colour.parse("{Red}"));
    }

    @Test
    public void testDoubleSingleColour() {
        assertArrayEquals(new Colour[]{Colour.WHITE}, Colour.parse("{White}{White}"));
        assertArrayEquals(new Colour[]{Colour.GREEN}, Colour.parse("{Green}{Green}"));
    }

    @Test
    public void testNoColour() {
        assertArrayEquals(new Colour[]{}, Colour.parse(""));
        assertArrayEquals(new Colour[]{}, Colour.parse("{Tap}"));
        assertArrayEquals(new Colour[]{}, Colour.parse("text"));
    }

    @Test
    public void testMultiColourIsInOrder() {
        assertArrayEquals(new Colour[]{Colour.WHITE,Colour.GREEN}, Colour.parse("{Green}{White}"));
        assertArrayEquals(new Colour[]{Colour.WHITE,Colour.GREEN}, Colour.parse("{White}{Green}"));
        assertArrayEquals(new Colour[]{Colour.WHITE,Colour.BLUE,Colour.BLACK,Colour.RED,Colour.GREEN},
                Colour.parse("{Blue}{White}{Green}{Black}{Red}"));
    }
}
