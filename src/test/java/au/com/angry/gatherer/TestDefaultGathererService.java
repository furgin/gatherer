package au.com.angry.gatherer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test the {@link DefaultGathererHttpService}
 */
public class TestDefaultGathererService
{
    private DefaultGathererService defaultGathererService;

    @Before
    public void setUp() throws Exception
    {
        defaultGathererService = new DefaultGathererService(new ResourceGathererHttpService());
    }

    /**
     * Getting this card will match with one pass.
     */
    @Test
    public void testGetCardSimpleCrumblingNecropolis()
    {
        MagicCard card = defaultGathererService.getCard("Crumbling Necropolis");
        assertEquals("Crumbling Necropolis", card.getName());
        assertEquals("Land", card.getTypes());
        assertEquals("Crumbling Necropolis enters the battlefield tapped.\n" +
                "{Tap}: Add {Blue}, {Black}, or {Red} to your mana pool.", card.getText());

        Iterable<MagicCardPrinting> printings = card.getPrintings();
//        assertEquals(2,printings.length);
//
//        assertEquals("Shards of Alara", printings[0].getMagicCardSet().getName());
//        assertEquals("ALA",printings[0].getMagicCardSet().getSymbol());
//        assertEquals(Rarity.UNCOMMON,printings[0].getRarity());
//        assertEquals(175112,printings[0].getMultiverseId());
//
//        assertEquals("Duel Decks: Ajani vs. Nicol Bolas",printings[1].getMagicCardSet().getName());
//        assertEquals("DDH",printings[1].getMagicCardSet().getSymbol());
//        assertEquals(Rarity.UNCOMMON,printings[1].getRarity());
//        assertEquals(259265,printings[1].getMultiverseId());
    }

    /**
     * Getting this card will match with one pass.
     */
    @Test
    public void testGetCardSimpleSerraAngel()
    {
        MagicCard card = defaultGathererService.getCard("Serra Angel");
        assertEquals("Serra Angel", card.getName());
        assertEquals("Creature - Angel", card.getTypes());
        assertEquals("Flying\n" +
                "Vigilance (Attacking doesn't cause this creature to tap.)", card.getText());
        assertEquals("4/4", card.getPt());
        assertEquals("{3}{White}{White}", card.getCost());
//        assertEquals(Colour.WHITE, Colour.parse(card.getCost()));

//        MagicCardPrinting[] printings = card.getPrintings();
//        assertEquals(15,printings.length);
    }

    /**
     * Getting this card will take two passes.
     */
    @Test
    public void testGetCardTwoPassNecropolis()
    {
        MagicCard card = defaultGathererService.getCard("Necropolis");
        assertEquals("Necropolis", card.getName());
        assertEquals("Artifact Creature - Wall", card.getTypes());
        assertEquals("0/1", card.getPt());
        assertEquals("{5}", card.getCost());

//        MagicCardPrinting[] printings = card.getPrintings();
//        assertEquals(1,printings.length);
//        assertEquals("The Dark",printings[0].getMagicCardSet().getName());
//        assertEquals("DK",printings[0].getMagicCardSet().getSymbol());
    }

    @Test
    public void testGetSet() throws Exception
    {
        assertEquals("ARB", defaultGathererService.getSet("Alara Reborn").getSymbol());
    }

    @Test
    public void testGetSets() {
        defaultGathererService.getSets();
    }

}
