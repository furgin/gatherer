package au.com.angry.gatherer;

import au.com.angry.gatherer.text.MagicTextFragment;
import au.com.angry.gatherer.text.MagicTextParser;
import au.com.angry.gatherer.text.SymbolFragment;
import au.com.angry.gatherer.text.TextFragment;
import com.google.common.collect.Iterables;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestMagicTextParser
{
    @Test
    public void testMagicParserNoSymbols() {
        assertEquals(1, Iterables.size(MagicTextParser.parse("test")));
    }

    @Test
    public void testMagicParserSymbol1() {
        final Iterable<MagicTextFragment> parse = MagicTextParser.parse("{White}");
        assertEquals(1, Iterables.size(parse));
        assertEquals(new SymbolFragment(Symbol.MANA_WHITE), Iterables.get(parse, 0));
    }

    @Test
    public void testMagicParserSymbol2() {
        final Iterable<MagicTextFragment> parse = MagicTextParser.parse("{White}{Red}");
        assertEquals(2, Iterables.size(parse));
        assertEquals(new SymbolFragment(Symbol.MANA_WHITE), Iterables.get(parse, 0));
        assertEquals(new SymbolFragment(Symbol.MANA_RED), Iterables.get(parse, 1));
    }

    @Test
    public void testMagicParserSymbolWithText() {
        final Iterable<MagicTextFragment> parse = MagicTextParser.parse("{Blue}text{Red}");
        assertEquals(3, Iterables.size(parse));
        assertEquals(new SymbolFragment(Symbol.MANA_BLUE), Iterables.get(parse, 0));
        assertEquals(new TextFragment("text"), Iterables.get(parse, 1));
        assertEquals(new SymbolFragment(Symbol.MANA_RED), Iterables.get(parse, 2));
    }
}
