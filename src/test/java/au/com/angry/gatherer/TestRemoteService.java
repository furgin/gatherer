package au.com.angry.gatherer;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 */
public class TestRemoteService
{
    private DefaultGathererService defaultGathererService;

    @Test
    public void testNothing()
    {
    }

    @Before
    public void setUp() throws Exception
    {
        defaultGathererService = new DefaultGathererService(new DefaultGathererHttpService());
    }

    @Test
    public void testTheros() throws Exception
    {
        final MagicCardSet theros = defaultGathererService.getSet("Theros");
        assertEquals("Theros",theros.getName());
        assertEquals(234,Iterables.size(theros.getCards()));
    }

    @Test
    public void testFlipCard() throws Exception
    {
        final MagicCard card = defaultGathererService.getCard("Huntmaster of the Fells");
        assertEquals("Huntmaster of the Fells", card.getName());
        assertEquals("2/2", card.getPt());
        assertEquals("{2}{Red}{Green}", card.getCost());
        assertEquals("Creature - Human Werewolf", card.getTypes());

        final Iterable<MagicCardPrinting> printings1 = card.getPrintings();
        assertEquals(1,Iterables.size(printings1));
        assertEquals("140a",printings1.iterator().next().getNumber());

        final MagicCard flipped = defaultGathererService.getCard("Ravager of the Fells");
        assertEquals("Ravager of the Fells", flipped.getName());
        assertEquals("4/4", flipped.getPt());
        assertEquals("", flipped.getCost());
        assertEquals("Creature - Werewolf", flipped.getTypes());

        final Iterable<MagicCardPrinting> printings2 = flipped.getPrintings();
        assertEquals(1,Iterables.size(printings2));
        assertEquals("140b",printings2.iterator().next().getNumber());

        final MagicCardSet set = defaultGathererService.getSet("Dark Ascension");
        boolean foundHuntmaster = false;
        boolean foundRavager = false;
        for (MagicCardPrinting p : set.getCards())
        {
            final String name = p.getMagicCard().getName();
            if(name.equals("Huntmaster of the Fells")) {
                assertEquals("140a",p.getNumber());
                foundHuntmaster = true;
            }
            if(name.equals("Ravager of the Fells")) {
                assertEquals("140b",p.getNumber());
                foundRavager = true;
            }
            if(foundHuntmaster&&foundRavager)
                break;
        }
        assertTrue(foundHuntmaster);
        assertTrue(foundRavager);
    }

    @Test
    public void testPhyrexianMana() throws Exception
    {
        MagicCardSet newPhyrexia = defaultGathererService.getSet("New Phyrexia");
        for (MagicCardPrinting printing : newPhyrexia.getCards())
        {
            if (printing.getMagicCard().getName().equals("Act of Aggression"))
            {
                assertEquals("{3}{Phyrexian Red}{Phyrexian Red}", printing.getMagicCard().getCost());
                break;
            }
        }

        final MagicCard card = defaultGathererService.getCard("Act of Aggression");
        assertEquals("Act of Aggression", card.getName());
        assertEquals("{3}{Phyrexian Red}{Phyrexian Red}", card.getCost());
        assertEquals("Instant", card.getTypes());
    }

//    @Test
//    public void testGetSet() throws Exception
//    {
//        MagicCardSet set = defaultGathererService.getSet("Apocalypse");
//        assertNotNull(set);
//        assertEquals(148, Iterables.size(set.getCards()));
//        for (MagicCardPrinting printing : set.getCards()) {
//            MagicCard card = printing.getMagicCard();
//            if(card.getName().contains("//")) { // split card
//                MagicCard splitCard = defaultGathererService.getCard(card.getName());
//                System.out.println(splitCard.getName()+": "+splitCard.getCost()+" "+printing.getPart());
//            }
//        }
//    }

    public void testImportSomeSets() throws Exception
    {
//        importSet("Limited Edition Alpha", 290);
//        importSet("Limited Edition Beta", 292);
//        importSet("Unlimited Edition", 292);
//        importSet("Revised Edition", 296);
//        importSet("Fourth Edition", 368);
//        importSet("Archenemy", 142, 0);
    }

    @Test
    public void testSplitCards()
    {
        final MagicCard wax = defaultGathererService.getCard("Wax // Wane (Wax)");
        final MagicCard wane = defaultGathererService.getCard("Wax // Wane (Wane)");
        assertEquals("{Green}", wax.getCost());
        assertEquals("{White}", wane.getCost());

        MagicCardSet set = defaultGathererService.getSet("Archenemy");
        boolean foundWax = false;
        boolean foundWane = false;
        for (MagicCardPrinting printing : set.getCards())
        {
            MagicCard card = printing.getMagicCard();
            if (card.getName().equals("Wax // Wane (Wax)"))
            {
                assertEquals("{Green}", card.getCost());
                foundWax = true;
            }
            if (card.getName().equals("Wax // Wane (Wane)"))
            {
                assertEquals("{White}", card.getCost());
                foundWane = true;
            }
            if (foundWax && foundWane)
                break;
        }
        assertTrue(foundWax);
        assertTrue(foundWane);

    }

    private void importSet(String name, int expected)
    {
        importSet(name, expected, 10);
    }

    private void importSet(String name, int expected, int max)
    {
        MagicCardSet set = defaultGathererService.getSet(name);
        assertNotNull(set);
        assertEquals(expected, Iterables.size(set.getCards()));
        System.out.println(set.getSymbol());
        int i = 0;
        for (MagicCardPrinting printing : set.getCards())
        {
            MagicCard card = printing.getMagicCard();
            final Iterable<MagicCardPrinting> printings = card.getPrintings();
            System.out.println(card.getName() + ": " + card.getCost());
            for (MagicCardPrinting p : printings)
                System.out.println(" " + p.getMagicCardSet().getName() + ": " + p.getRarity());
            i++;
            if (max > 0 && i > max)
                return;
        }
    }

    @Test
    public void testCardMultiplePrintings() throws Exception
    {
        final MagicCard card = defaultGathererService.getCard("Urza's Mine");
        for (MagicCardPrinting p : card.getPrintings())
            System.out.println(p.getMagicCardSet().getName() + ": " + p.getMultiverseId());
    }

    @Test
    public void testSetMultiplePrintings() throws Exception
    {
        final MagicCardSet set = defaultGathererService.getSet("Antiquities");
        for (MagicCardPrinting p : set.getCards())
            System.out.println(p.getMagicCardSet().getName() + ": " + p.getMultiverseId() + " " + p.getMagicCard().getName());
        System.out.println(Iterators.size(set.getCards().iterator()));
    }

//    @Test
//    public void testCardNumber() throws Exception
//    {
//        final MagicCardSet set = defaultGathererService.getSet("Magic 2013");
//        for (MagicCardPrinting p : set.getCards())
//            System.out.println(p.getMagicCardSet().getName() + ": " + p.getNumber() + " " + p.getMagicCard().getName());
//        System.out.println(Iterators.size(set.getCards().iterator()));
//    }

    private void dump(MagicCard card)
    {
        StringBuilder buf = new StringBuilder();
        buf.append(card.getName());
        buf.append(" - ");
        buf.append(card.getCost());
        buf.append(" - ");
        buf.append(card.getTypes());
        buf.append(" - ");
        for (Iterator<MagicCardPrinting> i = card.getPrintings().iterator(); i.hasNext(); )
        {
            MagicCardPrinting printing = i.next();
            buf.append(printing.getMagicCardSet().getSymbol());
            if (i.hasNext())
                buf.append(",");
        }
        System.out.println(buf.toString());
    }

}
