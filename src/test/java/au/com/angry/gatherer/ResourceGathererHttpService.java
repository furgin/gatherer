package au.com.angry.gatherer;

import java.io.InputStream;

/**
 * Loads html from local resources
 */
public class ResourceGathererHttpService implements GathererHttpService
{

    public InputStream search(String search)
    {
        return getClass().getClassLoader().getResourceAsStream("gatherer/" + search.replaceAll(" ", "") + ".html");
    }

    public InputStream get(long multiverseid, String part)
    {
        return getClass().getClassLoader().getResourceAsStream("gatherer/" + multiverseid + ".html");
    }

    public InputStream get(long multiverseid)
    {
        return getClass().getClassLoader().getResourceAsStream("gatherer/" + multiverseid + ".html");
    }

    public InputStream advanced()
    {
        return getClass().getClassLoader().getResourceAsStream("gatherer/Advanced.html");
    }

    public InputStream simple()
    {
        return getClass().getClassLoader().getResourceAsStream("gatherer/Simple.html");
    }

    public InputStream set(String set, String output)
    {
        return search(set);
    }

    public InputStream set(String set, String output, int page) {
        return set(set,output);
    }
}
