package au.com.angry.document;

/**
 */
public class DefaultDocumentSource implements DocumentSource
{
    public DocumentHolder load(DocumentLocation location)
    {
        return new DefaultDocumentHolder(location);
    }
}
