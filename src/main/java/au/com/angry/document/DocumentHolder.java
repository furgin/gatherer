package au.com.angry.document;

import org.jsoup.select.Elements;

/**
 */
public interface DocumentHolder
{
    public Elements select(String select);
}
