package au.com.angry.document;

import org.jsoup.nodes.Document;

/**
 */
public interface DocumentLocation
{
    Document getDocument();
}
