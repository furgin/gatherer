package au.com.angry.document;

/**
 */
public class DocumentLocationException extends RuntimeException
{
    public DocumentLocationException()
    {
    }

    public DocumentLocationException(String message)
    {
        super(message);
    }

    public DocumentLocationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DocumentLocationException(Throwable cause)
    {
        super(cause);
    }

}
