package au.com.angry.document;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 */
public class DefaultDocumentHolder implements DocumentHolder
{
    private Document document;
    private final DocumentLocation location;

    public DefaultDocumentHolder(DocumentLocation location)
    {
        this.location = location;
    }

    public Elements select(String select)
    {
        Elements elements = getDocument().select(select);
        if(elements.isEmpty()) {
            loadDocument();
            elements = getDocument().select(select);
        }
        return elements;
    }

    public Document getDocument()
    {
        if(document==null)
            loadDocument();
        return document;
    }

    private void loadDocument()
    {
        document = location.getDocument();
    }
}
