package au.com.angry.document;

/**
 */
public interface DocumentSource
{
    DocumentHolder load(DocumentLocation location);
}
