package au.com.angry.gatherer;

import au.com.angry.gatherer.text.MagicTextFragment;
import au.com.angry.gatherer.text.MagicTextParser;
import au.com.angry.gatherer.text.SymbolFragment;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import static au.com.angry.gatherer.Symbol.*;

/**
 * The five magic colours in order
 */
public enum Colour
{
    WHITE(MANA_WHITE, MANA_PHYREXIAN_WHITE),
    BLUE(MANA_BLUE, MANA_PHYREXIAN_BLUE),
    BLACK(MANA_BLACK, MANA_PHYREXIAN_BLACK),
    RED(MANA_RED, MANA_PHYREXIAN_RED),
    GREEN(MANA_GREEN, MANA_PHYREXIAN_GREEN);
    private final List<Symbol> symbols;

    public static Colour[] parse(String cost)
    {
        EnumSet<Colour> set = EnumSet.noneOf(Colour.class);
        final Iterable<MagicTextFragment> fragments = MagicTextParser.parse(cost);
        for (MagicTextFragment fragment : fragments)
        {
            if(fragment instanceof SymbolFragment) {
                SymbolFragment ssf = (SymbolFragment) fragment;
                for (Colour colour : values())
                {
                    if(colour.symbols.contains(ssf.getSymbol()))
                        set.add(colour);
                }
            }
        }
        return set.toArray(new Colour[set.size()]);
    }

    Colour(Symbol... symbols)
    {
        this.symbols = Arrays.asList(symbols);
    }
}
