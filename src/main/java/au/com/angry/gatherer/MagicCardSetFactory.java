package au.com.angry.gatherer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public final class MagicCardSetFactory
{
    private MagicCardSetFactory()
    {
    }

    public static MagicCardSet fromCardPage(final GathererService gathererService, final String name, final String symbol)
    {
        return new MagicCardSet()
        {
            private MagicCardSet gathererServiceSet;

            public String getName()
            {
                return name;
            }

            public String getSymbol()
            {
                return symbol;
            }

            public Iterable<MagicCardPrinting> getCards()
            {
                return gathererServiceSet.getCards();
            }

            public MagicCardSet getGathererServiceSet()
            {
                if (gathererServiceSet == null)
                    gathererServiceSet = gathererService.getSet(name);
                return gathererServiceSet;
            }
        };
    }

    public static MagicCardSet fromSetPage(final GathererService gathererService, final GathererHttpService gathererHttpService,
                                           final String setName, final Document doc)
    {
        return new MagicCardSet()
        {
            private List<MagicCardPrinting> cards;

            public String getName()
            {
                return setName;
            }

            public String getSymbol()
            {
                Element img = doc.select("tr.cardItem").first().select("td").last().select("img").first();
                String src = img.attr("src");
                src = src.substring(src.indexOf("set=") + 4);
                src = src.substring(0, src.indexOf("&"));
                return src;
            }

            public Iterable<MagicCardPrinting> getCards()
            {
                final MagicCardSet magicCardSet = this;
                if (cards == null)
                {
                    try
                    {
                        cards = new ArrayList<MagicCardPrinting>();

                        Document compactDoc = Jsoup.parse(gathererHttpService.set(setName, "spoiler"),
                                DefaultGathererService.CHARSET_NAME, DefaultGathererService.BASE_URL);

                        final Elements rows = compactDoc.select("div.textspoiler tr");
                        Map<String, Element> fields = new HashMap<String, Element>();
                        for (Element row : rows)
                        {
                            Elements tds = row.select("td");
                            if(tds.size()==2) {
                                String name = tds.get(0).text().trim();
                                if(name.contains(":"))
                                    name = name.substring(0,name.lastIndexOf(":"));
                                fields.put(name, tds.get(1));
                            } else {
                                cards.add(MagicCardPrintingFactory.fromSpoiler(gathererService, magicCardSet, fields));
                                fields = new HashMap<String, Element>();
                            }
                        }

                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
                return cards;
            }
        };
    }
}
