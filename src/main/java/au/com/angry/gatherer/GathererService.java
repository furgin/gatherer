package au.com.angry.gatherer;

/**
 */
public interface GathererService
{
    MagicCard getCard(String cardName);
    MagicCardSet getSet(String setName);
    MagicCardPrinting getPrinting(long multiverseId,String cardName);
}
