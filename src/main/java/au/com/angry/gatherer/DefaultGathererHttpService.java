package au.com.angry.gatherer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.StringTokenizer;

/**
 */
public class DefaultGathererHttpService implements GathererHttpService
{
    private static final Logger log = LoggerFactory.getLogger(DefaultGathererHttpService.class);

    public InputStream set(String set, String output)
    {
        log.debug("retrieve set [ {} ] output [ {} ]", set, output);
        try
        {
            return new URL("http://gatherer.wizards.com/Pages/Search/Default.aspx?sort=cn+&output=" + output + "&method=text&set=[" + quoted(set) + "]").openStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public InputStream set(String set, String output, int page)
    {
        log.debug("retrieve set [ {} ] output [ {} ] page [ {} ]", new Object[]{set, output, String.valueOf(page)});
        try
        {
            return new URL("http://gatherer.wizards.com/Pages/Search/Default.aspx?sort=cn+&page=" + page + "&output=" + output + "&set=[" + quoted(set) + "]").openStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private String quoted(String set)
    {
        try
        {
            return URLEncoder.encode("\"" + set + "\"", "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }

    public InputStream search(String search)
    {
        log.debug("search [ {} ]", search);
        try
        {
            return new URL("http://gatherer.wizards.com/Pages/Search/Default.aspx?name=" + createSearchTerm(search)).openStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public InputStream get(long multiverseid)
    {
        log.debug("get [ {} ]", multiverseid);
        try
        {
            return new URL("http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=" + multiverseid).openStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public InputStream get(long multiverseid, String part)
    {
        if(part==null)
            return get(multiverseid);

        log.debug("get [ {} ] part [ {} ]", multiverseid, part);
        try
        {
            return new URL("http://gatherer.wizards.com/Pages/Card/Details.aspx?Part="+part+"&multiverseid=" + multiverseid).openStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public InputStream advanced()
    {
        log.debug("advanced");
        try
        {
            return new URL("http://gatherer.wizards.com/Pages/Advanced.aspx").openStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public InputStream simple()
    {
        log.debug("simple");
        try
        {
            return new URL("http://gatherer.wizards.com/Pages/Default.aspx").openStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private String createSearchTerm(String name)
    {
        try
        {
            StringBuilder buf = new StringBuilder();
            StringTokenizer tok = new StringTokenizer(name);
            while (tok.hasMoreTokens())
            {
                String token = tok.nextToken();
                buf.append("+[");
                buf.append(URLEncoder.encode(token, "UTF-8"));
                buf.append("]");
            }
            return buf.toString();
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }
}
