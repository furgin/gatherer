package au.com.angry.gatherer;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public final class MagicCardFactory
{
    /**
     * Load card information from the card information page.
     *
     * @param gathererService the service to load any related data
     * @param name            the name of the card
     * @param doc             the dom of the card page
     * @return a card
     */
    public static MagicCard fromCardPage(final GathererService gathererService, final String name, final Document doc)
    {
        return new MagicCard()
        {
            private static final String FIELD_COST = "Mana Cost:";
            private static final String FIELD_TYPES = "Types:";
            private static final String FIELD_TEXT = "Card Text:";
            private static final String FIELD_PT = "P/T:";
            private static final String FIELD_FLAVOR = "Flavor Text:";

            private final Map<String, String> values = new HashMap<String, String>();

            public Element getCardTable()
            {
                if(SplitName.isSplit(name)) {
                    return doc.select("table.cardDetails:has(div.value:contains("+SplitName.parse(name).getCurrent()+"))").first();
                } else {
                    return doc.select("table.cardDetails:has(div.value:contains("+name+"))").first();
                }
            }

            public String getName()
            {
                return name;
            }

            public String getTypes()
            {
                return getField(FIELD_TYPES);
            }

            public String getText()
            {
                return getField(FIELD_TEXT);
            }

            public String getPt()
            {
                return getField(FIELD_PT).replace(" ","");
            }

            public String getCost()
            {
                return getField(FIELD_COST);
            }

            public String getFlavor()
            {
                return getField(FIELD_FLAVOR);
            }

            public Iterable<MagicCardPrinting> getPrintings()
            {
                Element fieldElement = getFieldElement("All Sets:");

                if (fieldElement == null)
                    fieldElement = getFieldElement("Other Sets:");

                if (fieldElement == null)
                    fieldElement = getFieldElement("Expansion:");

                Elements elements = fieldElement.select("img");
                List<MagicCardPrinting> printings = new ArrayList<MagicCardPrinting>();

                for (Element element : elements)
                    printings.add(MagicCardPrintingFactory.fromCardPage(gathererService, this, element, getCardTable()));

                return printings;
            }

            private Element getFieldElement(String field)
            {

                Elements elements = getCardTable().select("div.label");
                int i = 0;
                for (Element element : elements)
                {
                    String fieldName = element.text().trim();
                    if (fieldName.equals(field))
                        return element.parent().select("div.value").first();
                }
                return null;
            }

            private String getField(String field)
            {
                if (values.isEmpty())
                {
                    Elements elements = getCardTable().select("div.label");
                    int i = 0;
                    for (Element element : elements)
                    {
                        String fieldName = element.text().trim();
                        String val = "";
                        if (fieldName.equals(FIELD_COST) || fieldName.equals(FIELD_TEXT))
                            val = extractText(getCardTable().select("div.value").get(i));
                        else
                            val = getCardTable().select("div.value").get(i).text().trim();
                        val = val.replaceAll("\u2014", "-");
                        values.put(fieldName, val);
                        i++;
                    }
                }
                return values.containsKey(field) ? values.get(field) : "";
            }


        };
    }

    private MagicCardFactory()
    {
    }

    public static MagicCard fromChecklist(final GathererService gathererService, final Element element)
    {
        return new MagicCard()
        {
            private MagicCard defaultMagicCard;

            private MagicCard getMagicCard()
            {
                if (defaultMagicCard == null)
                {
                    defaultMagicCard = gathererService.getCard(getName());
                }
                return defaultMagicCard;
            }

            public String getName()
            {
                return element.select("td.name").text().trim();
            }

            public String getTypes()
            {
                return getMagicCard().getTypes();
            }

            public String getText()
            {
                return getMagicCard().getText();
            }

            public String getPt()
            {
                return getMagicCard().getPt();
            }

            public String getCost()
            {
                return getMagicCard().getCost();
            }

            public String getFlavor()
            {
                return getMagicCard().getFlavor();
            }

            public Iterable<MagicCardPrinting> getPrintings()
            {
                return getMagicCard().getPrintings();
            }
        };

    }

    public static MagicCard fromSpoiler(final GathererService gathererService, final Map<String, Element> fields)
    {
        return new MagicCard()
        {
            private MagicCard magicCard;

            private MagicCard getMagicCard()
            {
                if (magicCard == null)
                    magicCard = gathererService.getCard(getName());
                return magicCard;
            }

            public String getName()
            {
                return fields.get("Name").text().trim();
            }

            public String getTypes()
            {
                return fields.get("Type").text().trim().replace('\u2014', '-');
            }

            public String getText()
            {
                return getMagicCard().getText();
            }

            public String getPt()
            {
                return getMagicCard().getPt();
            }

            public String getCost()
            {
                return getMagicCard().getCost();
            }

            public String getFlavor()
            {
                return getMagicCard().getFlavor();
            }

            public Iterable<MagicCardPrinting> getPrintings()
            {
                return getMagicCard().getPrintings();
            }
        };
    }

    private static String extractText(Node root)
    {
        List<Node> children = root.childNodes();
        StringBuilder buf = new StringBuilder();
        for (Node child : children)
        {
            if (child.nodeName().equals("img"))
            {
                buf.append("{").append(child.attr("alt")).append("}");
            }
            else if (child.nodeName().equals("br")) {
                buf.append("\n");
            }
            else if (child.nodeName().equals("div")) {
                buf.append(extractText(child) + "\n");
            }
            else if (child.childNodes().size() > 0)
            {
                buf.append(extractText(child)).append("\n");
            }
            else
            {
                buf.append(child.toString());
            }
        }
        return buf.toString().trim();
    }

}
