package au.com.angry.gatherer.text;

import au.com.angry.gatherer.Symbol;

/**
 * Fragment of text containing a symbol
 */
public class SymbolFragment implements MagicTextFragment
{
    private final Symbol symbol;

    public SymbolFragment(Symbol symbol)
    {
        this.symbol = symbol;
    }

    public Symbol getSymbol()
    {
        return symbol;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SymbolFragment that = (SymbolFragment) o;
        if (symbol != that.symbol) return false;
        return true;
    }

    public int hashCode()
    {
        return symbol != null ? symbol.hashCode() : 0;
    }
}
