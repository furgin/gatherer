package au.com.angry.gatherer.text;

import au.com.angry.gatherer.Symbol;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class MagicTextParser
{
    private MagicTextParser(){}

    private enum Parsing
    {
        TEXT, SYMBOL
    }

    public static Iterable<MagicTextFragment> parse(String text)
    {
        List<MagicTextFragment> fragments = new ArrayList<MagicTextFragment>();
        Parsing state = Parsing.TEXT;
        StringBuilder current = new StringBuilder();

        for (int i = 0; i < text.length(); i++)
        {
            final char c = text.charAt(i);

            switch (state)
            {
                case TEXT:
                    if (c == '{')
                    {
                        if (current.length() > 0)
                        {
                            fragments.add(new TextFragment(current.toString()));
                            current.setLength(0);
                        }
                        state = Parsing.SYMBOL;
                    } else {
                        current.append(c);
                    }
                    break;
                case SYMBOL:
                    if(c=='}') {
                        if (current.length() > 0)
                        {
                            fragments.add(new SymbolFragment(Symbol.fromToken(current.toString())));
                            current.setLength(0);
                        }
                        state = Parsing.TEXT;
                    } else {
                        current.append(c);
                    }
                    break;
            }

        }

        switch (state)
        {
            case TEXT:
                if(current.length()>0) {
                    fragments.add(new TextFragment(current.toString()));
                    current.setLength(0);
                }
                break;
            case SYMBOL:
                throw new IllegalArgumentException("unterminated symbol : {"+current.toString());
        }

        return fragments;
    }
}
