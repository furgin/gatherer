package au.com.angry.gatherer.text;

/**
 * Simple text
 */
public class TextFragment implements MagicTextFragment
{
    private final String text;

    public TextFragment(String text)
    {
        this.text = text;
    }

    public String getText()
    {
        return text;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextFragment that = (TextFragment) o;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        return true;
    }

    public int hashCode()
    {
        return text != null ? text.hashCode() : 0;
    }
}
