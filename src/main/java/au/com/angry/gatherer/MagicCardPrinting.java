package au.com.angry.gatherer;

/**
 */
public interface MagicCardPrinting
{
    long getMultiverseId();
    String getNumber();
    MagicCard getMagicCard();
    MagicCardSet getMagicCardSet();
    Rarity getRarity();
}
