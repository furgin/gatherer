package au.com.angry.gatherer;

import java.io.InputStream;
import java.net.URL;

/**
 * Simple URL service for communicating with the gatherer service
 */
public interface GathererHttpService
{
    InputStream set(String set, String output);
    InputStream set(String set, String output, int page);
    InputStream search(String search);
    InputStream get(long multiverseid);
    InputStream get(long multiverseid, String part);
    InputStream advanced();
    InputStream simple();
}
