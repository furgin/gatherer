package au.com.angry.gatherer;

/**
 */
public enum Rarity
{
    COMMON("C"), UNCOMMON("U"), RARE("R"), LAND("L"), SPECIAL("S"), PROMO("P"), MYTHIC_RARE("M");

    private String symbol;

    Rarity(String symbol)
    {
        this.symbol = symbol;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public static Rarity fromSymbol(String symbol)
    {
        for (Rarity rarity : Rarity.values())
        {
            if (rarity.symbol.equals(symbol))
                return rarity;
        }
        return null;
    }

}
