package au.com.angry.gatherer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Map;
import java.util.StringTokenizer;

/**
 */
public final class MagicCardPrintingFactory
{
    private MagicCardPrintingFactory()
    {
    }

    public static MagicCardPrinting fromCardPage(final GathererService gathererService, final MagicCard magicCard,
                                                 final Element element, final Element cardTable )
    {
        return new MagicCardPrinting()
        {

            private MagicCardSet magicCardSet;

            public long getMultiverseId()
            {
                Element linkElement = element.parent();
                String mid = linkElement.attr("href");
                mid = mid.substring(mid.lastIndexOf("=") + 1);
                return Long.parseLong(mid);
            }

            public String getNumber()
            {
                final Element cardNumberElement = getFieldElement("Card #:");
                return cardNumberElement == null ? null : cardNumberElement.text();
            }

            public MagicCard getMagicCard()
            {
                return magicCard;
            }

            public MagicCardSet getMagicCardSet()
            {
                if (magicCardSet == null)
                {
                    String setName = element.attr("alt");
                    setName = setName.substring(0, setName.indexOf("("));
                    setName = setName.trim();
                    String symbol = element.attr("src");
                    symbol = symbol.substring(symbol.indexOf("set=") + 4);
                    symbol = symbol.substring(0, symbol.indexOf("&"));
                    magicCardSet = MagicCardSetFactory.fromCardPage(gathererService, setName, symbol);
                }
                return magicCardSet;
            }

            public Rarity getRarity()
            {
                String rarity = element.attr("src");
                rarity = rarity.substring(rarity.indexOf("rarity=") + 7);
                return Rarity.fromSymbol(rarity);
            }

            private Element getFieldElement(String field)
            {

                Elements elements = cardTable.select("div.label");
                for (Element element : elements)
                {
                    String fieldName = element.text().trim();
                    if (fieldName.equals(field))
                        return element.parent().select("div.value").first();
                }
                return null;
            }
        };
    }

    public static MagicCardPrinting fromSpoiler(final GathererService gathererService,
                                                final MagicCardSet magicCardSet,
                                                final Map<String, Element> fields)
    {
        return new MagicCardPrinting()
        {
            private MagicCard magicCard;
            private MagicCardPrinting fromCardPage;

            private MagicCardPrinting getMagicCardPrinting() {
                if(fromCardPage==null)
                    fromCardPage = gathererService.getPrinting(getMultiverseId(),fields.get("Name").text());
                return fromCardPage;
            }

            public long getMultiverseId()
            {
                Element linkElement = fields.get("Name").select("a").first();
                String mid = linkElement.attr("href");
                mid = mid.substring(mid.lastIndexOf("=") + 1);
                return Long.parseLong(mid);
            }

            public String getNumber()
            {
                return getMagicCardPrinting().getNumber();
            }

            public MagicCard getMagicCard()
            {
                if (magicCard == null)
                    magicCard = MagicCardFactory.fromSpoiler(gathererService, fields);
                return magicCard;
            }

            public MagicCardSet getMagicCardSet()
            {
                return magicCardSet;
            }

            public Rarity getRarity()
            {
                String setText = fields.get("Set/Rarity").text().trim();
                StringTokenizer tok = new StringTokenizer(setText,",");
                while (tok.hasMoreTokens())
                {
                    String token = tok.nextToken();
                    if(token.contains(magicCardSet.getName())) {
                        token = token.trim();
                        token = token.substring(magicCardSet.getName().length()).trim();
                        return Rarity.valueOf(token.toUpperCase().replace(" ","_"));
                    }
                }
                return null;
            }

        };
    }

}
