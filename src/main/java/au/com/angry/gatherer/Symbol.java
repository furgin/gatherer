package au.com.angry.gatherer;

/**
 * All known magic symbols.
 */
public enum Symbol
{
    MANA_PHYREXIAN_RED("R/P","Phyrexian Red"),
    MANA_PHYREXIAN_GREEN("G/P","Phyrexian Green"),
    MANA_PHYREXIAN_WHITE("W/P","Phyrexian White"),
    MANA_PHYREXIAN_BLACK("B/P","Phyrexian Black"),
    MANA_PHYREXIAN_BLUE("U/P","Phyrexian Blue"),

    MANA_RED("R","Red"),
    MANA_GREEN("G","Green"),
    MANA_WHITE("W","White"),
    MANA_BLACK("B","Black"),
    MANA_BLUE("U","Blue"),

    MANA_1("1","1"),
    MANA_2("2","2"),
    MANA_3("3","3"),
    MANA_4("4","4"),
    MANA_5("5","5"),
    MANA_6("6","6"),
    MANA_7("7","7"),
    MANA_8("8","8"),
    MANA_9("9","9"),
    MANA_10("10","10"),
    MANA_11("11","11"),
    MANA_12("12","12"),
    MANA_13("13","13"),
    MANA_14("14","14"),
    MANA_15("15","15"),
    MANA_16("16","16"),
    MANA_17("17","17"),
    MANA_18("18","18"),
    MANA_19("19","19");

    private Symbol(String human, String token)
    {
        this.human = human;
        this.token = token;
    }

    public final String human;
    public final String token;

    public static String humanToToken(String humanString) {
        String s = humanString;
        for (Symbol symbol : Symbol.values())
            s = s.replaceAll(symbol.human,symbol.token);
        return s;
    }

    public static Symbol fromToken(String token) {
        for (Symbol symbol : Symbol.values()) {
            if(symbol.token.equals(token)) {
                return symbol;
            }
        }
        return null;
    }

}
