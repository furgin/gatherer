package au.com.angry.gatherer;

/**
 */
public class SplitName
{
    public static boolean isSplit(String name)
    {
        return name.contains("//");
    }

    public static SplitName parse(String name)
    {
        String left = name.substring(0, name.indexOf("//")).trim();
        String right = name.substring(name.indexOf("//") + 2, name.indexOf("(")).trim();
        String current = name.substring(name.indexOf("(") + 1, name.indexOf(")")).trim();
        return new SplitName(left, right, current);
    }

    private final String left;
    private final String right;
    private final String current;

    public SplitName(String left, String right, String current)
    {
        this.current = current;
        this.left = left;
        this.right = right;
    }

    public String getLeft()
    {
        return left;
    }

    public String getRight()
    {
        return right;
    }

    public String getCurrent()
    {
        return current;
    }
}
