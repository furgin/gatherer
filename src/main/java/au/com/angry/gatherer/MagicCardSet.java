package au.com.angry.gatherer;

import java.net.URL;
import java.util.List;

/**
 */
public interface MagicCardSet
{
    public String getName();
    public String getSymbol();
    public Iterable<MagicCardPrinting> getCards();
}
