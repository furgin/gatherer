package au.com.angry.gatherer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**

 */
public class DefaultGathererService implements GathererService
{
    public static final String BASE_URL = "http://gatherer.wizards.com";
    public static final String CHARSET_NAME = "UTF-8";
    private final GathererHttpService gathererHttpService;

    public DefaultGathererService(GathererHttpService gathererHttpService)
    {
        this.gathererHttpService = gathererHttpService;
    }

    public MagicCardSet getSet(String setName)
    {
        try
        {
            Document standardDoc = Jsoup.parse(gathererHttpService.set(setName, "standard"), CHARSET_NAME, BASE_URL);
            return MagicCardSetFactory.fromSetPage(this, gathererHttpService, setName, standardDoc);
        }
        catch (IOException e)
        {
            throw new RuntimeException();
        }
    }

    public MagicCardPrinting getPrinting(long multiverseId, String cardName)
    {
        try
        {
            Document doc = Jsoup.parse(gathererHttpService.get(multiverseId), CHARSET_NAME, BASE_URL);
            Element imgElement = doc.select("img[alt=" + cardName + "]").first();
            final Element cardTable = doc.select("table.cardDetails:has(div.value:contains(" + cardName + "))").first();
            return MagicCardPrintingFactory.fromCardPage(this, getCard(cardName), imgElement, cardTable);
        }
        catch (IOException e)
        {
            throw new RuntimeException();
        }
    }

    public MagicCard getCard(String cardName)
    {
        try
        {
            String searchTerm = cardName;

            String part = null;
            if (SplitName.isSplit(cardName))
            {
                SplitName split = SplitName.parse(cardName);
                searchTerm = split.getLeft() + " " + split.getRight();
                part = split.getCurrent();
            }

            Document doc = Jsoup.parse(gathererHttpService.search(searchTerm), CHARSET_NAME, BASE_URL);
            String cardUrl = find(cardName, doc);
            if (cardUrl != null)
            {
                String field = "multiverseid=";
                String substring = cardUrl.substring(cardUrl.indexOf(field) + field.length());
                if (substring.contains("&"))
                    substring = substring.substring(0, substring.indexOf("&"));
                if (part == null)
                    doc = Jsoup.parse(gathererHttpService.get(Long.parseLong(substring)), CHARSET_NAME, BASE_URL);
                else
                    doc = Jsoup.parse(gathererHttpService.get(Long.parseLong(substring), part), CHARSET_NAME, BASE_URL);
            }
            return MagicCardFactory.fromCardPage(this, cardName, doc);
        }
        catch (IOException e)
        {
            throw new RuntimeException();
        }
    }

    public List<String> getSets()
    {
        List<String> list = new ArrayList<String>();
        try
        {
            Document doc = Jsoup.parse(gathererHttpService.advanced(), CHARSET_NAME, BASE_URL);
            final Elements elements = doc.select("div[id~=setAddText] a");
            for (Element element : elements)
                list.add(element.text());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        return list;
    }

    private static String find(String cardName, Document doc)
    {
        Elements cardItems = doc.select("span.cardTitle a");
        for (Element cardItem : cardItems)
        {
            String text = cardItem.text();
            if (text.equals(cardName))
                return cardItem.attr("href");
        }
        return null;
    }

}
