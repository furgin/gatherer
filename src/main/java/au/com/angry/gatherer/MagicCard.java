package au.com.angry.gatherer;

/**
 */
public interface MagicCard
{
    String getName();

    String getTypes();

    String getText();

    String getPt();

    String getCost();

    String getFlavor();

    Iterable<MagicCardPrinting> getPrintings();
}
